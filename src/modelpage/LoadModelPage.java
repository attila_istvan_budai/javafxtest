package modelpage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.When;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import view.WriteNumbersModel;

import com.bme.datamining.ann.dnn.DNNOpenCL;
import com.bme.datamining.filereader.LoadModel;
import com.bme.datamining.filewriter.SaveModel;

public class LoadModelPage {

	private final TreeTableView<Model> table = new TreeTableView<>();
	private ObservableList<Model> data;

	String root = "./models";

	public TreeTableView initTable(WriteNumbersModel writeNumbersModel) {

		table.setRowFactory(new Callback<TreeTableView<Model>, TreeTableRow<Model>>() {
			@Override
			public TreeTableRow<Model> call(TreeTableView<Model> tableView) {
				final TreeTableRow<Model> row = new TreeTableRow<>();
				final ContextMenu rowMenu = new ContextMenu();
				MenuItem loadItem = new MenuItem("Load Model");
				loadItem.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						String fileName = row.getItem().getFileName();
						System.out.println(fileName);
						DNNOpenCL dnn = LoadModel.loadModel(new File(root, fileName));
						writeNumbersModel.setDnn(dnn);
					}
				});
				rowMenu.getItems().addAll(loadItem);

				// only display context menu for non-null items:
				row.contextMenuProperty().bind(new When(Bindings.isNotNull(row.itemProperty())).then(rowMenu).otherwise((ContextMenu) null));
				return row;
			}
		});

		TreeItem<Model> root = new TreeItem<Model>();
		table.setRoot(root);
		table.setShowRoot(false);

		List<String> filenames = getAllFiles();
		List<Model> models = initializeFolders(filenames);

		data = FXCollections.observableArrayList(models);

		root.setExpanded(true);
		data.stream().forEach((data) -> {
			root.getChildren().add(new TreeItem<>(data));
		});
		for (TreeItem<Model> m : root.getChildren()) {
			List<Model> subModels = getSubFolders(m.getValue().getFileName());
			for (Model subModel : subModels) {
				TreeItem<Model> subItem = new TreeItem<Model>(subModel);
				m.getChildren().add(subItem);
			}
		}
		TreeTableColumn<Model, String> firstNameCol = new TreeTableColumn<>("File Name");
		firstNameCol.setCellValueFactory((TreeTableColumn.CellDataFeatures<Model, String> param) -> new ReadOnlyStringWrapper(param.getValue().getValue().getFileName()));

		TreeTableColumn<Model, String> visibleUnitsColumn = new TreeTableColumn<>("Visible Units");
		visibleUnitsColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Model, String> param) -> new ReadOnlyStringWrapper(param.getValue().getValue().getVisibleUnits()));

		TreeTableColumn<Model, String> hiddenUnitsColumn = new TreeTableColumn<>("Hidden Units");
		hiddenUnitsColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Model, String> param) -> new ReadOnlyStringWrapper(param.getValue().getValue().getHiddentUnits()));

		table.getColumns().addAll(firstNameCol, visibleUnitsColumn, hiddenUnitsColumn);

		/*
		 * for (int i = 1; i <= 5; i++) { TableColumn layer = new
		 * TableColumn("Layer " + Integer.toString(i)); TableColumn
		 * hiddenUnitNumber = new TableColumn("Hidden Units"); TableColumn
		 * visibleUnitNumber = new TableColumn("Visible Units");
		 * 
		 * hiddenUnitNumber.setCellValueFactory(new
		 * PropertyValueFactory<>("visibleUnits"));
		 * visibleUnitNumber.setCellValueFactory(new
		 * PropertyValueFactory<>("hiddenUnits"));
		 * 
		 * layer.getColumns().addAll(visibleUnitNumber, hiddenUnitNumber); //
		 * table.getColumns().add(layer); }
		 */

		// table.setItems(data);

		// firstNameCol.setSortType(TableColumn.SortType.DESCENDING);

		final VBox vbox = new VBox();
		vbox.setSpacing(5);
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(table);
		return table;
	}

	private List<String> getAllFiles() {
		// Directory path here
		String path = root;

		String files;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		List<String> filenames = new ArrayList<String>();

		for (int i = 0; i < listOfFiles.length; i++) {
			files = listOfFiles[i].getName();
			filenames.add(files);
		}

		return filenames;

	}

	private Model loadLayer(String dir, String fName, int level) {
		int visible = 0;
		int hidden = 0;
		File meta = new File(dir, "model.meta");
		if (meta.exists()) {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(meta.getPath()), "utf-8"))) {
				String line = reader.readLine();
				if (line != null) {
					String num = line.substring(SaveModel.strVisibleNumber.length(), line.length());
					visible = Integer.parseInt(num);
					line = reader.readLine();
					num = line.substring(SaveModel.strHiddenNumber.length(), line.length());
					hidden = Integer.parseInt(num);
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new Model(Integer.toString(level) + " Layer", Integer.toString(visible), Integer.toString(hidden));
		// model.addHiddenUnit(hidden);
		// model.addVisibleUnit(visible);

	}

	private List<Model> initializeFolders(List<String> foldernames) {
		List<Model> models = new ArrayList<Model>();
		for (String name : foldernames) {
			Model model = new Model(name, "", "");
			models.add(model);
		}
		return models;

	}

	private List<Model> getSubFolders(String foldername) {
		List<Model> models = new ArrayList<Model>();
		File folder = new File(root, foldername);
		File[] fList = folder.listFiles();
		for (int i = 0; i < fList.length; i++) {
			String subfolder = fList[i].getPath();
			Model model = loadLayer(subfolder, foldername, i + 1);
			models.add(model);
		}

		return models;

	}

	public static class Model {

		private final SimpleStringProperty fileName;
		private final SimpleStringProperty visibleUnits;
		private final SimpleStringProperty hiddentUnits;

		public Model(String fileName, String visibleUnits, String hiddentUnits) {
			this.fileName = new SimpleStringProperty(fileName);
			this.visibleUnits = new SimpleStringProperty(visibleUnits);
			this.hiddentUnits = new SimpleStringProperty(hiddentUnits);
		}

		public String getFileName() {
			return fileName.getValue();
		}

		public String getVisibleUnits() {
			return visibleUnits.getValue();
		}

		public String getHiddentUnits() {
			return hiddentUnits.getValue();
		}

	}
}
