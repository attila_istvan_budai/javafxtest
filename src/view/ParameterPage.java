package view;

import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import com.bme.datamining.ann.dnn.DNNOpenCL;
import com.bme.datamining.ann.rbm.RBMLayer;

public class ParameterPage {

	TextField iterationNumberText;
	TextField learningRateText;
	List<Integer> layers;
	List<TextField> txtHiddenUnits;
	GridPane grid;

	private int levels;

	public List<Integer> getHiddenUnits() {
		return layers;
	}

	public int getIterationNumber() {
		return Integer.parseInt(iterationNumberText.getText());
	}

	public float getLearningRate() {
		return Float.parseFloat(learningRateText.getText());
	}

	public HBox initializeBaseParams() {
		HBox hbox = new HBox();
		// Label hiddenNumberLabel = new Label("Number of hidden neurons:");
		// hiddenNumberText = new TextField();
		// hiddenNumberText.setText("10");

		Label iterationNumberLabel = new Label("Number of iterations:");
		iterationNumberText = new TextField();
		iterationNumberText.setText("250");

		Label learningRateLabel = new Label("Learning rate:");
		learningRateText = new TextField("0.01");
		learningRateText.setText("0.01");

		hbox.getChildren().addAll(iterationNumberLabel, iterationNumberText, learningRateLabel, learningRateText);

		return hbox;
	}

	public GridPane initializeLayers() {
		txtHiddenUnits = new ArrayList<TextField>();
		grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(0, 10, 0, 10));

		HBox hbox = new HBox();

		Button btnAddLayer = new Button();
		btnAddLayer.setText("Add new layer");
		btnAddLayer.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				levels++;
				addNewLayer();

			}

		});

		Button btnRemoveLayer = new Button();
		btnRemoveLayer.setText("Remove last layer");
		btnRemoveLayer.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				removeLastLayer();

			}

		});

		Button btnSaveLayer = new Button();
		btnSaveLayer.setText("Save layers");
		btnSaveLayer.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				saveLayers();

			}

		});
		hbox.getChildren().addAll(btnAddLayer, btnRemoveLayer, btnSaveLayer);
		grid.add(hbox, 0, 0);

		return grid;
	}

	private void saveLayers() {
		layers = new ArrayList<>();
		if (txtHiddenUnits.size() > 1) {
			for (int i = 0; i < txtHiddenUnits.size(); i++) {
				String txt = txtHiddenUnits.get(i).getText();
				try {
					layers.add(Integer.parseInt(txt));
				} catch (NumberFormatException e) {
					// TODO: warning
					System.out.println("invalid");
				}
			}
		}
	}

	private void removeLastLayer() {
		List<Node> children = grid.getChildren();
		if (children.size() > 1) {
			levels--;
			children.remove(children.size() - 1);
			txtHiddenUnits.remove(txtHiddenUnits.size() - 1);
		}
	}

	private void removeAll() {
		List<Node> children = grid.getChildren();
		levels = 0;
		children.clear();
		txtHiddenUnits.clear();
	}

	private void addNewLayer(int hidden) {
		TextField hiddenUnitNumber = new TextField();
		hiddenUnitNumber.setText(Integer.toString(hidden));
		addLayer(hiddenUnitNumber);
	}

	private void addNewLayer() {
		TextField hiddenUnitNumber = new TextField();
		addLayer(hiddenUnitNumber);

	}

	private void addLayer(TextField hiddenUnitNumber) {
		Text hiddenTxt = new Text("Hidden unit:");
		HBox hbox = new HBox();
		hbox.setSpacing(10);
		Text layerName = new Text("Layer " + Integer.toString(levels));

		txtHiddenUnits.add(hiddenUnitNumber);
		hbox.getChildren().addAll(layerName, hiddenTxt, hiddenUnitNumber);
		hbox.setAlignment(Pos.CENTER);

		Rectangle layer = new Rectangle();
		layer.setWidth(400);
		layer.setHeight(60);
		layer.setFill(Color.WHITESMOKE);
		layer.setStrokeWidth(3);
		layer.setStroke(Color.BLACK);

		StackPane stack = new StackPane();
		stack.getChildren().addAll(layer, hbox);
		StackPane.setAlignment(hbox, Pos.CENTER);

		grid.add(stack, 0, levels);
		levels++;
	}

	public void loadDNNModel(DNNOpenCL dnn) {
		if (dnn != null) {
			removeAll();
			for (RBMLayer layer : dnn.getLayers()) {
				addNewLayer(layer.hiddenUnitNumber);
			}
			saveLayers();
		}

	}
}
