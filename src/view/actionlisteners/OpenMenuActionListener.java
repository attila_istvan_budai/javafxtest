package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javax.swing.JFileChooser;

import view.WriteNumbersModel;

public class OpenMenuActionListener implements EventHandler<ActionEvent> {
	WriteNumbersModel model;

	public OpenMenuActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	@Override
	public void handle(ActionEvent arg0) {
		JFileChooser openFile = new JFileChooser();
		int choose = openFile.showOpenDialog(null);
		if (choose == 0) {
			model.readFromFile(openFile.getSelectedFile());
		}

	}
}