package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbers;
import view.WriteNumbersModel;

public class NextPanelsActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;
	WriteNumbers view;

	public NextPanelsActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	@Override
	public void handle(ActionEvent arg0) {
		view.setVisibleFrom(view.getVisibleFrom() + view.getPanelNumber());
		view.setVisibleTo(view.getVisibleTo() + view.getPanelNumber());
		model.setVisiblePanels(view.getVisibleFrom(), view.getVisibleTo());
		view.refreshAll();

	}
}