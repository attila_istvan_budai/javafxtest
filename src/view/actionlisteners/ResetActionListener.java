package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import newdynamicpanel.DynamicPanel;
import view.WriteNumbers;

public class ResetActionListener implements EventHandler<ActionEvent> {

	WriteNumbers view;

	public ResetActionListener(WriteNumbers view) {
		this.view = view;
	}

	@Override
	public void handle(ActionEvent arg0) {
		for (DynamicPanel p : view.getTrainingPanelView().getTrainingPanels()) {
			p.reset();
		}

	}
}
