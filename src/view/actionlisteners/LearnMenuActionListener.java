package view.actionlisteners;

import java.io.IOException;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbers;
import view.WriteNumbersModel;

public class LearnMenuActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;
	WriteNumbers view;

	public LearnMenuActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	class LearnWorker extends Task<Void> {

		@Override
		public Void call() throws Exception {
			model.learn();
			return null;
		}

		@Override
		public void done() {
			try {
				// List<float[]> result = model.learnFromModel();
				model.saveDnnResult();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// model.showModel();

		}
	}

	@Override
	public void handle(ActionEvent arg0) {
		try {
			LearnWorker worker = new LearnWorker();
			new Thread(worker).start();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

}
