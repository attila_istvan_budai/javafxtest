package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbers;

public class ResetTestActionListener implements EventHandler<ActionEvent> {

	WriteNumbers view;

	public ResetTestActionListener(WriteNumbers view) {
		this.view = view;
	}

	@Override
	public void handle(ActionEvent arg0) {
		view.getInputPane().reset();

	}
}