package view.actionlisteners;

import java.io.IOException;
import java.util.List;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbers;
import view.WriteNumbersModel;

public class LogRegMenuActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;
	WriteNumbers view;

	public LogRegMenuActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	class LearnWorker extends Task<Void> {

		@Override
		public Void call() throws IOException {
			System.out.println("learnFromModel");
			List<float[]> result = model.learnFromModel();
			System.out.println("started");
			return null;
		}

		@Override
		public void done() {
		}
	}

	@Override
	public void handle(ActionEvent arg0) {
		try {
			LearnWorker worker = new LearnWorker();
			new Thread(worker).start();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

}
