package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbersModel;

public class ReconstructModelActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;
	String newLine = System.getProperty("line.separator");

	public ReconstructModelActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	@Override
	public void handle(ActionEvent arg0) {
		model.reconstruct();
	}
}
