package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javax.swing.JFileChooser;

import view.WriteNumbersModel;

public class SaveMenuActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;

	public SaveMenuActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	@Override
	public void handle(ActionEvent arg0) {
		JFileChooser saveFile = new JFileChooser();
		int choose = saveFile.showSaveDialog(null);
		if (choose == 0) {
			model.writeToFile(saveFile.getSelectedFile());
		}

	}
}
