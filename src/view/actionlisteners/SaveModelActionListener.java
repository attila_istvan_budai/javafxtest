package view.actionlisteners;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbersModel;

public class SaveModelActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;

	public SaveModelActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	@Override
	public void handle(ActionEvent arg0) {
		model.saveModelToFile(new File("G:\\modelResult.dat"));

	}
}