package view.actionlisteners;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbersModel;

public class LoadModelActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;

	public LoadModelActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	@Override
	public void handle(ActionEvent arg0) {
		model.loadModelFromFile(new File("G:\\modelResult.dat"));
		model.showModel();
	}
}
