package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;
import view.MyBarChart;
import view.WriteNumbersModel;
import view.WriteNumbersModel.Result;

public class CheckMenuActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;
	Text resultLabel;
	MyBarChart barchart;
	String newLine = System.getProperty("line.separator");

	public CheckMenuActionListener(WriteNumbersModel model, Text resultLabel, MyBarChart barchart) {
		this.model = model;
		this.resultLabel = resultLabel;
		this.barchart = barchart;
	}

	@Override
	public void handle(ActionEvent arg0) {
		Result result = model.check();
		resultLabel.setText("" + (int) result.prediction);
		barchart.createSeries(result.resultMap);

		// StringBuffer strBuffer = new StringBuffer();
		// strBuffer.append("Result: " + newLine);
		// for (Entry<Integer, Float> entry : result.resultMap.entrySet()) {
		// strBuffer.append(entry.getKey() + " " + entry.getValue() + newLine);
		// }
		// resultLabel.setText(strBuffer.toString());
	}
}
