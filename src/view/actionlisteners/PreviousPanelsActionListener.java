package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbers;
import view.WriteNumbersModel;

public class PreviousPanelsActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;
	WriteNumbers view;

	public PreviousPanelsActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	@Override
	public void handle(ActionEvent arg0) {
		if (view.getVisibleFrom() > view.getPanelNumber()) {
			view.setVisibleFrom(view.getVisibleFrom() - view.getPanelNumber());
			view.setVisibleTo(view.getVisibleTo() - view.getPanelNumber());

		} else {
			view.setVisibleFrom(0);
			view.setVisibleTo(view.getPanelNumber());
		}
		model.setVisiblePanels(view.getVisibleFrom(), view.getVisibleTo());
		view.refreshAll();

	}
}
