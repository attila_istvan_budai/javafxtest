package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbers;
import view.WriteNumbersModel;

public class OpenMniskMenuActionListener implements EventHandler<ActionEvent> {

	WriteNumbersModel model;
	WriteNumbers view;

	public OpenMniskMenuActionListener(WriteNumbersModel model, WriteNumbers view) {
		this.model = model;
		this.view = view;
	}

	@Override
	public void handle(ActionEvent arg0) {
		// view.setTrainingPanelsEditable(false);
		model.readFromMnist();
		view.refreshAll();

	}
}
