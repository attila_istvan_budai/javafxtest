package view.actionlisteners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbersModel;

public class GetRandomInputActionListener implements EventHandler<ActionEvent> {
	WriteNumbersModel model;

	public GetRandomInputActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	@Override
	public void handle(ActionEvent arg0) {
		model.setRandomInput();

	}
}