package view.actionlisteners;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.WriteNumbersModel;

public class SaveDNNModelActionListener implements EventHandler<ActionEvent> {
	WriteNumbersModel model;

	public SaveDNNModelActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	@Override
	public void handle(ActionEvent arg0) {
		try {
			model.saveDnnResult();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
}
