package view.actionlisteners;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javax.swing.JFileChooser;

import view.WriteNumbersModel;

public class OpenDNNModelActionListener implements EventHandler<ActionEvent> {
	WriteNumbersModel model;

	public OpenDNNModelActionListener(WriteNumbersModel model) {
		this.model = model;
	}

	@Override
	public void handle(ActionEvent arg0) {
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("choosertitle");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);

		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
			try {
				model.loadDnnResult(chooser.getSelectedFile());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}
}
