package view.components;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import newdynamicpanel.DynamicPanel;
import view.WriteNumbers;
import view.WriteNumbersModel;
import view.actionlisteners.NextPanelsActionListener;
import view.actionlisteners.PreviousPanelsActionListener;

public class TrainingPanelView extends HBox {

	List<DynamicPanel> trainingPanels;

	public TrainingPanelView(WriteNumbersModel model, WriteNumbers view) {

		this.setHeight(280);
		ScrollPane controlsPanel = new ScrollPane();
		controlsPanel.setContent(this);

		this.setSpacing(10);

		Button btnPrevious = new Button();
		btnPrevious.setText("Prev");
		btnPrevious.setOnAction(new PreviousPanelsActionListener(model, view));

		Button btnNext = new Button();
		btnNext.setText("Next");
		btnNext.setOnAction(new NextPanelsActionListener(model, view));

		// controls.setLayout(new GridLayout(1, view.getPanelNumber()));
		this.getChildren().add(btnPrevious);
		trainingPanels = new ArrayList<DynamicPanel>();

		for (int i = 0; i < view.getPanelNumber(); i++) {
			// trainPanel.setSize(300, 300);
			DynamicPanel p = new DynamicPanel.DynamicPanelBuilder().setSizes(10, 10).setCellNumber(view.getWidthOfPanels(), view.getHeightOfPanels())
					.editable(true).createDynamicPanel();
			trainingPanels.add(p);
			this.getChildren().add(p);
		}

		btnPrevious.setPrefSize(50, this.getHeight());
		btnNext.setPrefSize(50, this.getHeight());
		this.getChildren().add(btnNext);

	}

	public List<DynamicPanel> getTrainingPanels() {
		return trainingPanels;
	}

	public void setTrainingPanels(List<DynamicPanel> trainingPanels) {
		this.trainingPanels = trainingPanels;
	}

}
