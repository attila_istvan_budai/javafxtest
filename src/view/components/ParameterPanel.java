package view.components;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import view.WriteNumbers;
import view.WriteNumbersModel;
import view.actionlisteners.GetRandomInputActionListener;
import view.actionlisteners.LearnMenuActionListener;
import view.actionlisteners.ResetActionListener;
import view.actionlisteners.ResetTestActionListener;

public class ParameterPanel extends HBox {

	ProgressBar progressbar = new ProgressBar(0);

	public ParameterPanel(WriteNumbersModel model, WriteNumbers view) {

		this.setHeight(15);

		Button btnLearn = new Button();
		btnLearn.setText("Learn");
		btnLearn.setOnAction(new LearnMenuActionListener(model, view));

		Button btnReset = new Button();
		btnReset.setText("Reset");
		btnReset.setOnAction(new ResetActionListener(view));

		Button btnClearResult = new Button();
		btnClearResult.setText("Clear");
		btnClearResult.setOnAction(new ResetTestActionListener(view));

		Button btnRandomInputFromMnist = new Button();
		btnRandomInputFromMnist.setText("Random Input");
		btnRandomInputFromMnist.setOnAction(new GetRandomInputActionListener(model));

		// progressbar = new ProgressBar(0);
		// progressbar.setPrefHeight(15);

		// setAllFontStyle(hiddenNumberLabel, hiddenNumberText,
		// iterationNumberLabel, iterationNumberText, learningRateLabel,
		// learningRateText, btnLearn, btnReset, btnClearResult,
		// btnRandomInputFromMnist, progressbar);
		this.getChildren().addAll(btnLearn, btnReset, btnClearResult, btnRandomInputFromMnist, progressbar);

	}

	public void setFontStyle(Label lbl) {
		lbl.setFont(new Font("Arial", this.getHeight()));
	}

	public void setFontStyle(TextField txt) {
		txt.setFont(new Font("Arial", this.getHeight()));
	}

	public void setAllFontStyle(Control... controls) {
		for (Control c : controls) {
			c.setPrefHeight(this.getHeight());
			if (c instanceof Label) {
				((Label) c).setFont(new Font("Arial", this.getHeight()));
				((Label) c).setAlignment(Pos.BOTTOM_CENTER);
			} else if (c instanceof TextField) {
				((TextField) c).setFont(new Font("Arial", this.getHeight()));
			} else if (c instanceof Button) {
				((Button) c).setFont(new Font("Arial", this.getHeight()));
			}
		}
	}

	public ProgressBar getProgressBar() {
		return progressbar;
	}

}
