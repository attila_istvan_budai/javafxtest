package view.components;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import view.WriteNumbers;
import view.WriteNumbersModel;
import view.actionlisteners.LearnMenuActionListener;
import view.actionlisteners.LoadModelActionListener;
import view.actionlisteners.LogRegMenuActionListener;
import view.actionlisteners.OpenDNNModelActionListener;
import view.actionlisteners.OpenMenuActionListener;
import view.actionlisteners.OpenMniskMenuActionListener;
import view.actionlisteners.SaveDNNModelActionListener;
import view.actionlisteners.SaveMenuActionListener;
import view.actionlisteners.SaveModelActionListener;

public class MyMenubar extends MenuBar {

	Menu menu;
	Menu mnistMenu;
	Menu dnnMenu;

	MenuItem learn;
	MenuItem open;
	MenuItem save;

	MenuItem openMnist;
	MenuItem saveModel;
	MenuItem loadModel;
	MenuItem learnFromModel;

	MenuItem openDNNModel;
	MenuItem saveDNNModel;

	public MyMenubar(WriteNumbersModel model, WriteNumbers view) {
		menu = new Menu("File..");
		mnistMenu = new Menu("Mnist..");
		dnnMenu = new Menu("DNN..");

		this.getMenus().addAll(menu, mnistMenu, dnnMenu);

		learn = new MenuItem("Learn");
		learn.setOnAction(new LearnMenuActionListener(model, view));

		open = new MenuItem("Open");
		open.setOnAction(new OpenMenuActionListener(model));

		save = new MenuItem("Save");
		save.setOnAction(new SaveMenuActionListener(model));

		menu.getItems().addAll(learn, open, save);

		openMnist = new MenuItem("Open Mnist");
		openMnist.setOnAction(new OpenMniskMenuActionListener(model, view));

		saveModel = new MenuItem("Save Model");
		saveModel.setOnAction(new SaveModelActionListener(model));

		loadModel = new MenuItem("Load Model");
		loadModel.setOnAction(new LoadModelActionListener(model));

		learnFromModel = new MenuItem("Learn From Model");
		learnFromModel.setOnAction(new LogRegMenuActionListener(model, view));

		mnistMenu.getItems().addAll(openMnist, saveModel, loadModel, learnFromModel);

		openDNNModel = new MenuItem("Load model");
		openDNNModel.setOnAction(new OpenDNNModelActionListener(model));

		saveDNNModel = new MenuItem("Save model");
		saveDNNModel.setOnAction(new SaveDNNModelActionListener(model));

		dnnMenu.getItems().addAll(openDNNModel, saveDNNModel);

	}

}
