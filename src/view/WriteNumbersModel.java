package view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import mnistreader.MinstDatasetReader;
import mnistreader.MinstItem;
import newdynamicpanel.DynamicPanel;
import newdynamicpanel.DynamicPanelModel;
import utils.MathUtils;
import utils.RBMViewObserver;

import com.bme.datamining.ann.activatefunctions.SigmoidFunction;
import com.bme.datamining.ann.dnn.DNNOpenCL;
import com.bme.datamining.ann.dnn.DNNOpenCL.DNNBuilder;
import com.bme.datamining.ann.logisticregression.LogisticRegression;
import com.bme.datamining.ann.neurons.InnerNeuron;
import com.bme.datamining.ann.weights.Weight;
import com.bme.datamining.filereader.FloatList;
import com.bme.datamining.filereader.LoadModel;
import com.bme.datamining.filewriter.SaveModel;
import com.bme.datamining.logging.observer.simpleobserver.RBMResultObject;

public class WriteNumbersModel {
	WriteNumbers view;
	private float[] weight;
	// RestrictedBoltzmannMachineOpenCL rbm;
	List<DynamicPanelModel> panelModels;
	DNNOpenCL dnn;
	DNNBuilder dnnBuilder;
	MinstItem[] mnistItems;
	MinstItem[] mnistTestItems;

	int hiddenNumber;
	int iterationNumber;
	float learningRate;

	List<Integer> hiddenUnits;

	List<float[]> logregModel;

	List<List<RBMResultObject>> resultLists;

	public static final int mnistNumber = 5000;

	public WriteNumbersModel(WriteNumbers view) {
		this.view = view;
		panelModels = new ArrayList<DynamicPanelModel>();
		dnnBuilder = new DNNBuilder();
	}

	public DNNOpenCL getDnn() {
		return dnn;
	}

	public void setDnn(DNNOpenCL dnn) {
		this.dnn = dnn;
	}

	public WriteNumbers getView() {
		return view;
	}

	public int getHiddenNumber() {
		return hiddenNumber;
	}

	public int getIterationNumber() {
		return iterationNumber;
	}

	public float getLearningRate() {
		return learningRate;
	}

	public void writeToFile(File file) {
		Writer writer;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getAbsolutePath()), "utf-8"));
			List<DynamicPanel> panels = view.getTrainingPanelView().getTrainingPanels();
			for (DynamicPanel t : panels) {
				writer.write(t.getModel().toString());
				writer.write("\n");
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void saveModelToFile(File file) {
		Writer writer;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getAbsolutePath()), "utf-8"));
			for (int i = 0; i < getWeight().length; i++) {
				writer.write(getWeight()[i] + " ");
				if (i % hiddenNumber == 0 && i != 0) {
					writer.write("\n");
				}
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void loadModelFromFile(File file) {
		setParameters();
		String line;
		int index = 0;
		FloatList list = new FloatList();

		try {
			BufferedReader br = new BufferedReader(new java.io.FileReader(file.getPath()));
			do {
				line = br.readLine();
				if (line == null || line.equals("")) {

				} else {
					String[] words = line.split(" ");
					for (String w : words) {
						list.add(Float.parseFloat(w));
						index++;
					}
				}

			} while (line != null);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setWeight(list.toArray());
	}

	public void setRandomInput() {
		Random r = new Random();
		float[] data = mnistItems[r.nextInt(mnistItems.length - 1)].data;

		DynamicPanel pane = view.getInputPane();
		pane.getModel().setModel(data, 28, 28);
		pane.updateCells(data, 1);
	}

	public void readFromMnist() {
		panelModels = new ArrayList<DynamicPanelModel>();
		File labelFile = new File("C:\\mnist\\train-labels-idx1-ubyte.gz");
		File imageFile = new File("C:\\mnist\\train-images-idx3-ubyte.gz");
		MinstDatasetReader reader = new MinstDatasetReader(labelFile, imageFile);
		mnistItems = new MinstItem[mnistNumber];
		mnistTestItems = new MinstItem[mnistNumber];
		for (int i = 0; i < mnistNumber; i++) {
			MinstItem item = reader.getTrainingItem((i % 10));
			mnistItems[i] = item;
			float[] data = item.data;

			DynamicPanelModel.DynamicPanelModelBuilder builder = new DynamicPanelModel.DynamicPanelModelBuilder();
			builder.setCellNumber(28, 28);
			builder.setModel(MathUtils.normalizeWithValues(data, 255, 0));
			// builder.setModel(data);
			panelModels.add(builder.createDynamicPanel());
		}

		for (int j = mnistNumber; j < mnistNumber * 2; j++) {
			MinstItem item = reader.getTrainingItem((j % 10));
			mnistTestItems[j - mnistNumber] = item;
		}
		System.out.println(mnistNumber + " number of data loaded");
		setVisiblePanels(0, 5);

	}

	public void setVisiblePanels(int from, int to) {
		List<DynamicPanel> panels = view.getTrainingPanelView().getTrainingPanels();
		for (int i = from; i < to; i++) {
			panels.get(i - from).setModel(panelModels.get(i));
		}
	}

	public void readFromFile(File file) {
		try {
			BufferedReader br = new BufferedReader(new java.io.FileReader(file.getPath()));
			List<DynamicPanel> panels = view.getTrainingPanelView().getTrainingPanels();
			String line;
			int column = 0, row = 0, matrixNumber = 0;
			FloatList input = new FloatList();

			do {
				line = br.readLine();
				if (line == null || line.equals("")) {
					panels.get(matrixNumber).getModel().setModel(input.toArray(), column, row);
					column = 0;
					row = 0;

					input = new FloatList();

					matrixNumber++;
				} else {
					String[] words = line.split("  ");
					column = words.length;
					for (String w : words) {
						input.add(Float.parseFloat(w));
					}
					row++;
				}

			} while (line != null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void learn() throws Exception {
		resultLists = new ArrayList<List<RBMResultObject>>();
		setParameters();
		learningRate = view.getParameterPage().getLearningRate();
		iterationNumber = view.getParameterPage().getIterationNumber();
		List<Integer> hiddenUnits = view.getParameterPage().getHiddenUnits();
		int numberOfElements = panelModels.size();
		int arrayLength = panelModels.get(0).getModel().length;
		float[] trainingSet = new float[numberOfElements * arrayLength];
		for (int i = 0; i < numberOfElements; i++) {
			float[] currentArray = panelModels.get(i).getModel();
			System.arraycopy(currentArray, 0, trainingSet, i * arrayLength, arrayLength);
		}

		if (dnn == null) {
			DNNBuilder builder = dnnBuilder.addFirstLayer(hiddenUnits.get(0), arrayLength);
			if (hiddenUnits.size() > 1) {
				for (int i = 1; i < hiddenUnits.size(); i++) {
					dnnBuilder.addLayer(hiddenUnits.get(i));
				}
			}
			dnn = builder.createSingleton();
		}

		dnn.registerObserver(new RBMViewObserver(this));
		// rbm = new RestrictedBoltzmannMachineOpenCL();
		// rbm.registerObserver(new RBMViewObserver(this));
		// setWeight(rbm.train(trainingSet, arrayLength, hiddenNumber,
		// numberOfElements, iterationNumber, learningRate));
		setWeight(dnn.train(trainingSet, numberOfElements, iterationNumber, learningRate, 0.5f, 50, 0.1f, numberOfElements, null));
	}

	public void addElementToResultList(List<RBMResultObject> list) {
		resultLists.add(list);
	}

	public void saveDnnResult() throws IOException {
		if (logregModel != null) {
			SaveModel.saveNewModel(dnn, resultLists);
		}
	}

	public void loadDnnResult(File folderName) throws IOException {
		dnn = LoadModel.loadModel(folderName);
	}

	public List<float[]> learnFromModel() throws IOException {
		logregModel = new ArrayList<>();
		List<Integer> hiddenUnits = view.getParameterPage().getHiddenUnits();
		hiddenNumber = hiddenUnits.get(hiddenUnits.size() - 1);
		System.out.println("learnFromModel()");
		System.out.println(hiddenNumber);
		LogisticRegression logreg = new LogisticRegression(new InnerNeuron(new SigmoidFunction()));
		// logreg.registerObserver(new LogRegObserver(this));
		float[] trainingLabels = new float[mnistNumber];
		float[] testLabels = new float[mnistNumber];
		float[] constructedData = new float[mnistNumber * hiddenNumber];
		float[] testDataMatrix = new float[mnistNumber * hiddenNumber];
		int step = 2500;
		for (int d = 0; d < mnistItems.length; d += step) {
			int upperBound = d + step < mnistItems.length ? d + step : mnistItems.length;
			for (int i = d; i < upperBound; i++) {
				float[] data = mnistItems[i].data;
				float[] constructed = dnn.calculateHiddenLayer(data, data.length / (28 * 28));
				data = null;
				System.arraycopy(constructed, 0, constructedData, i * hiddenNumber, hiddenNumber);

				float[] testData = mnistTestItems[i].data;
				float[] constructedTestRow = dnn.calculateHiddenLayer(testData, testData.length / (28 * 28));
				testData = null;
				System.arraycopy(constructedTestRow, 0, testDataMatrix, i * hiddenNumber, hiddenNumber);

			}
			for (int currentNumber = 0; currentNumber < 10; currentNumber++) {
				String current = Integer.toString(currentNumber);
				for (int i = 0; i < mnistItems.length; i++) {
					if (mnistItems[i].label.equals(current)) {
						testLabels[i] = 1;
					} else {
						testLabels[i] = 0;
					}

					if (mnistTestItems[i].label.equals(current)) {
						testLabels[i] = 1;
					} else {
						testLabels[i] = 0;
					}
				}
				Weight w = null;
				if (logregModel.size() > currentNumber) {
					w = new Weight(logregModel.get(currentNumber));
					logregModel.set(currentNumber,
							logreg.learn(constructedData, trainingLabels, testDataMatrix, testLabels, trainingLabels.length, hiddenNumber, testLabels.length, 0.01f, 2000, w));
				} else {
					logregModel.add(logreg.learn(constructedData, trainingLabels, testDataMatrix, testLabels, trainingLabels.length, hiddenNumber, testLabels.length, 0.01f, 2000,
							w));
				}
				// logregModel.add(logreg.learnCL(constructedData,
				// trainingLabels,
				// trainingLabels.length, hiddenNumber, 0.01f, 2000));
				System.out.println("=======================================");
			}
		}

		return logregModel;
	}

	public void reconstruct() {
		DynamicPanel pane = view.getInputPane();
		float[] data = pane.getModel().getModel();
		float[] resultdata = dnn.reconstructData(data, 1);
		DynamicPanel resultPane = view.getResultPane();
		resultPane.getModel().setModel(resultdata, 28, 28);
		resultPane.updateCells(resultdata, 1);
	}

	public void setParameters() {
		learningRate = view.getParameterPage().getLearningRate();
		iterationNumber = view.getParameterPage().getIterationNumber();
		hiddenUnits = view.getParameterPage().getHiddenUnits();
	}

	public void showModel() {

		DynamicPanel modelResultPanel = view.getModelResultPane();
		float[] arr = MathUtils.normalizeArray(getWeight());
		float[] filtered = MathUtils.filterByPositiveValues(arr, 0.5f);
		// showSeperatedModel(filtered);
		modelResultPanel.updateCells(filtered, hiddenNumber);
	}

	public Result check() {
		DynamicPanel pane = view.getInputPane();
		float[] data = pane.getModel().getModel();
		Map<Integer, Float> probs = new TreeMap<Integer, Float>();

		float prediction = dnn.predict(data, 1);
		System.out.println(prediction);

		Result res = new Result();
		res.resultMap = probs;
		res.prediction = prediction;

		return res;
	}

	public class Result {
		public Map<Integer, Float> resultMap;
		public float prediction;
	}

	static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
		SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
				int res = e2.getValue().compareTo(e1.getValue());
				return res != 0 ? res : 1;
			}
		});
		sortedEntries.addAll(map.entrySet());
		return sortedEntries;
	}

	public float[] getWeight() {
		return weight;
	}

	public void setWeight(float[] weight) {
		this.weight = weight;
	}

	public void addElementToLogRegResultGraph(double d, int iteration) {
		if (view.getCharts() != null) {
			view.getCharts().addDataToLogRegLineChart(d, iteration);
		}
	}

	public void addElementToResultGraph(double d, int iteration) {
		if (view.getCharts() != null) {
			view.getCharts().addDataToLineChart(d, iteration);
			view.getCharts().addDataToDeltaRmseChart(d, iteration);
		}
	}

	public void addNewSeriesToResultGraph() {
		if (view.getCharts() != null) {
			view.getCharts().addNewLineChart();
		}
	}

	public void addNewSeriesToLogRegResultGraph() {
		if (view.getCharts() != null) {
			view.getCharts().addNewLogRegLineChart();
		}
	}

	public void calculateWeightHistogram() {
		if (view.getCharts() != null) {
			view.getCharts().recalculateHistogram(getWeight(), 10);
		}
	}

}
