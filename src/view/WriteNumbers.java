package view;

import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import modelpage.LoadModelPage;
import newdynamicpanel.DynamicPanel;
import newdynamicpanel.DynamicPanelModel;
import tabs.DraggableTab;
import view.actionlisteners.CheckMenuActionListener;
import view.actionlisteners.ReconstructModelActionListener;
import view.components.MyMenubar;
import view.components.ParameterPanel;
import view.components.TrainingPanelView;
import charts.ChartPage;

public class WriteNumbers extends Application {

	private ParameterPanel parameterPanel;
	private TrainingPanelView trainingPanelView;

	List<DynamicPanel> detailedResultPanels;
	DynamicPanel inputPane;
	DynamicPanel resultPane;
	DynamicPanel modelResultPane;
	ChartPage charts;
	ParameterPage parameterPage;

	WriteNumbersModel model;

	// JLabel lblGuessTheNumber;

	int widthOfPanels = 28;
	int heightOfPanels = 28;

	private final int panelNumber = 5;
	private int visibleFrom = 0;
	private int visibleTo = getPanelNumber();
	private MyMenubar menuBar;

	public WriteNumbers() {
		model = new WriteNumbersModel(this);
	}

	public DynamicPanel getInputPane() {
		return inputPane;
	}

	public void setInputPane(DynamicPanel inputPane) {
		this.inputPane = inputPane;
	}

	public DynamicPanel getResultPane() {
		return resultPane;
	}

	public void setResultPane(DynamicPanel resultPane) {
		this.resultPane = resultPane;
	}

	public DynamicPanel getModelResultPane() {
		return modelResultPane;
	}

	public void setModelResultPane(DynamicPanel modelResultPane) {
		this.modelResultPane = modelResultPane;
	}

	public int getWidthOfPanels() {
		return widthOfPanels;
	}

	public void setWidthOfPanels(int widthOfPanels) {
		this.widthOfPanels = widthOfPanels;
	}

	public int getHeightOfPanels() {
		return heightOfPanels;
	}

	public void setHeightOfPanels(int heightOfPanels) {
		this.heightOfPanels = heightOfPanels;
	}

	public int getVisibleFrom() {
		return visibleFrom;
	}

	public void setVisibleFrom(int visibleFrom) {
		this.visibleFrom = visibleFrom;
	}

	public int getVisibleTo() {
		return visibleTo;
	}

	public void setVisibleTo(int visibleTo) {
		this.visibleTo = visibleTo;
	}

	public int getPanelNumber() {
		return panelNumber;
	}

	public ParameterPanel getParameterPanel() {
		return parameterPanel;
	}

	public void setParameterPanel(ParameterPanel parameterPanel) {
		this.parameterPanel = parameterPanel;
	}

	public TrainingPanelView getTrainingPanelView() {
		return trainingPanelView;
	}

	public void setTrainingPanelView(TrainingPanelView trainingPanelView) {
		this.trainingPanelView = trainingPanelView;
	}

	public ChartPage getCharts() {
		return charts;
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(final Stage primaryStage) {
		Platform.setImplicitExit(false);

		DraggableTab tab1 = new DraggableTab("Overview");
		tab1.setClosable(false);
		// tab1.setDetachable(false);
		VBox vbox = writeNumberGUI();
		tab1.setContent(vbox);
		DraggableTab tab2 = new DraggableTab("Results");
		tab2.setClosable(false);
		charts = new ChartPage();
		float[] data = charts.initializeNormalDistributedData(10000);
		Node node = charts.addWeightHistogram(data);
		Node node2 = charts.addRmseChart(data);
		Node node3 = charts.addDeltaRmseChart(data);
		Node node4 = charts.addLogRegRmseChart(data);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(0, 10, 0, 10));

		grid.add(node, 0, 0);
		grid.add(node2, 1, 0);
		grid.add(node3, 2, 0);
		grid.add(node4, 0, 1);
		tab2.setContent(grid);
		DraggableTab tab3 = new DraggableTab("Parameters");
		parameterPage = new ParameterPage();
		HBox top = parameterPage.initializeBaseParams();
		GridPane layers = parameterPage.initializeLayers();
		GridPane paramgrid = new GridPane();

		paramgrid.add(top, 0, 0);
		paramgrid.add(layers, 0, 1);
		tab3.setContent(paramgrid);
		tab3.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				// TODO Auto-generated method stub
				if (newValue)
					System.out.println("Textfield on focus");
				parameterPage.loadDNNModel(model.getDnn());

			}
		});

		LoadModelPage loadPage = new LoadModelPage();
		DraggableTab tab4 = new DraggableTab("Tab 4");

		tab4.setClosable(false);
		tab4.setContent(loadPage.initTable(model));
		TabPane tabs = new TabPane();
		tabs.getTabs().add(tab1);
		tabs.getTabs().add(tab2);
		tabs.getTabs().add(tab3);
		tabs.getTabs().add(tab4);

		StackPane root = new StackPane();
		root.getChildren().add(tabs);

		Scene scene = new Scene(root);
		scene.getStylesheets().add(WriteNumbers.class.getResource("application.css").toExternalForm());

		primaryStage.setScene(scene);
		primaryStage.show();

	}

	public DynamicPanel createPanel() {
		float[] data = new float[28 * 28];

		DynamicPanel panel = new DynamicPanel(28, 28, 10, 10, null);
		DynamicPanelModel.DynamicPanelModelBuilder builder = new DynamicPanelModel.DynamicPanelModelBuilder();

		builder.setCellNumber(28, 28);
		builder.setModel(data);
		DynamicPanelModel model = builder.createDynamicPanel();

		panel.setModel(model);

		panel.updateBasedOnModel();
		return panel;
	}

	public HBox addHBox(DynamicPanel... panels) {
		HBox hbox = new HBox();
		hbox.setPadding(new Insets(15, 12, 15, 12));
		hbox.setSpacing(10);
		hbox.setStyle("-fx-background-color: #336699;");

		hbox.getChildren().addAll(panels);

		return hbox;
	}

	public void refreshAll() {
		List<DynamicPanel> trainingPanels = getTrainingPanelView().getTrainingPanels();
		for (DynamicPanel p : trainingPanels) {
			p.updateBasedOnModel();
		}
	}

	public VBox writeNumberGUI() {

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(15, 12, 15, 12));
		vbox.setSpacing(10);
		// menus
		menuBar = new MyMenubar(model, this);

		// learning parameters
		setParameterPanel(new ParameterPanel(model, this));

		// draw panels
		setTrainingPanelView(new TrainingPanelView(model, this));

		vbox.getChildren().addAll(menuBar, parameterPanel, trainingPanelView, resultPane());

		return vbox;
	}

	public HBox resultPane() {

		HBox hbox = new HBox();
		// menus
		inputPane = new DynamicPanel.DynamicPanelBuilder().setSizes(10, 10).setCellNumber(widthOfPanels, heightOfPanels).editable(true).setEmptyModel(1).createDynamicPanel();
		resultPane = new DynamicPanel.DynamicPanelBuilder().setSizes(10, 10).setCellNumber(widthOfPanels, heightOfPanels).editable(false).setEmptyModel(1).createDynamicPanel();

		Text resultText = new Text("");
		resultText.setFont(Font.font("Verdana", 256));
		resultText.setFill(Color.RED);

		MyBarChart barchart = new MyBarChart();
		FlowPane pane = barchart.addWeightHistogram();

		VBox vbox = new VBox();
		Button btnCheckResult = new Button();
		btnCheckResult.setText("Check result" + "\u25BA");
		btnCheckResult.setOnAction(new CheckMenuActionListener(model, resultText, barchart));

		Button btnReconstruct = new Button();
		btnReconstruct.setText("Reconstruct" + "\u25BA");
		btnReconstruct.setOnAction(new ReconstructModelActionListener(model));

		vbox.getChildren().addAll(btnCheckResult, btnReconstruct);

		hbox.getChildren().addAll(inputPane, vbox, resultPane, resultText, pane);

		return hbox;
	}

	public ParameterPage getParameterPage() {
		return parameterPage;
	}

}