package view;

import java.util.Map;
import java.util.Map.Entry;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

public class MyBarChart {

	ObservableList<XYChart.Series<String, Number>> barChartSeries;
	BarChart<String, Number> bc;

	public FlowPane addWeightHistogram() {
		FlowPane flow = setUpFloawPane();

		final CategoryAxis xAxis = new CategoryAxis();
		final NumberAxis yAxis = new NumberAxis();
		bc = new BarChart(xAxis, yAxis);
		bc.setTitle("Weight distribution summary");
		xAxis.setLabel("Value");
		yAxis.setLabel("Number of element");

		// ObservableList<XYChart.Series<String, Number>> barChartSeries =
		// createSeries(null);
		bc.setData(barChartSeries);

		ImageView pages[] = new ImageView[8];
		for (int i = 0; i < 1; i++) {
			flow.getChildren().add(bc);
		}

		return flow;
	}

	public void createSeries(Map<Integer, Float> map) {
		System.out.println(map);
		ObservableList<XYChart.Series<String, Number>> list = FXCollections.observableArrayList();
		XYChart.Series series1 = new Series();
		series1.setName("Weight distribution");
		for (Entry<Integer, Float> entry : map.entrySet()) {
			series1.getData().add(new XYChart.Data(Float.toString(entry.getKey()), entry.getValue()));
			System.out.println(Float.toString(entry.getKey()) + " " + entry.getValue());
		}

		list.add(series1);
		bc.setData(barChartSeries);

	}

	private FlowPane setUpFloawPane() {
		FlowPane flow = new FlowPane();
		flow.setPadding(new Insets(5, 0, 5, 0));
		flow.setVgap(4);
		flow.setHgap(4);
		return flow;
	}
}
