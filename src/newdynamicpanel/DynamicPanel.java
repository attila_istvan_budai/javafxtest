package newdynamicpanel;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class DynamicPanel extends GridPane {

	private int columnNumber;
	private int rowNumber;
	private int hiddenNumber;
	private DynamicPanelModel model;

	private int width;
	private int height;

	private Rectangle[][] cells;

	public DynamicPanel(int columnNumber, int rowNumber, int width, int height, DynamicPanelModel model) {
		super();
		this.model = model;
		this.columnNumber = columnNumber;
		this.rowNumber = rowNumber;
		this.width = width;
		this.height = height;
		construct();

	}

	public void construct() {

		// 2D array of Buttons with value of 5,5
		cells = new Rectangle[columnNumber][rowNumber];

		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells.length; j++) {

				// Initializing 2D buttons with values i,j
				cells[i][j] = createElement();
				this.add(cells[i][j], i, j);
			}
		}

	}

	private Rectangle createElement() {
		Rectangle rectangle = new Rectangle(width, height);
		rectangle.setStroke(Color.BLACK);
		rectangle.setFill(Color.WHITE);
		EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent t) {
				if (t.getButton() == MouseButton.SECONDARY) {
					rectangle.setFill(Color.WHITE);
				} else if (t.getButton() == MouseButton.PRIMARY) {
					rectangle.setFill(Color.RED);
				}
			}
		};

		rectangle.setOnMouseClicked(mouseHandler);
		rectangle.setOnMouseDragged(mouseHandler);
		rectangle.setOnMouseEntered(mouseHandler);
		rectangle.setOnMouseExited(mouseHandler);
		rectangle.setOnMouseMoved(mouseHandler);
		rectangle.setOnMousePressed(mouseHandler);
		rectangle.setOnMouseReleased(mouseHandler);

		return rectangle;
	}

	public void updateBasedOnModel() {
		for (int col = 0; col < columnNumber; col++) {
			for (int row = 0; row < rowNumber; row++) {
				Color color = model.getCellColors().get(row * columnNumber + col);
				cells[col][row].setFill(color);
			}
		}
	}

	public void updateCells(float[] data, int hiddenNumber) {
		this.model.setModel(data, columnNumber, rowNumber);
		this.model.setHiddenNumber(hiddenNumber);
		this.model.calculateCellColors();
		updateBasedOnModel();
	}

	public void reset() {
		float[] data = new float[columnNumber * rowNumber];
		updateCells(data, 1);
	}

	public void setModel(DynamicPanelModel model) {
		this.model = model;
	}

	public DynamicPanelModel getModel() {
		return this.model;
	}

	public static class DynamicPanelBuilder {
		private int columnNumber = 10;
		private int rowNumber = 10;
		private int width = 250;
		private int height = 250;
		private DynamicPanelModel model;
		boolean isEditable;
		Color color;

		public DynamicPanelBuilder setEmptyModel(int hiddenNumber) {
			model = new DynamicPanelModel.DynamicPanelModelBuilder().setCellNumber(columnNumber, rowNumber).setHiddenNumber(hiddenNumber).createDynamicPanel();
			return this;
		}

		public DynamicPanelBuilder setCellNumber(int colNumber, int rowNumber) {
			this.columnNumber = colNumber;
			this.rowNumber = rowNumber;
			return this;
		}

		public DynamicPanelBuilder setSizes(int width, int height) {
			this.width = width;
			this.height = height;
			return this;
		}

		public DynamicPanelBuilder editable(boolean isEditable) {
			this.isEditable = isEditable;
			return this;
		}

		public DynamicPanelBuilder setColor(Color color) {
			this.color = color;
			return this;
		}

		public DynamicPanel createDynamicPanel() {
			return new DynamicPanel(columnNumber, rowNumber, width, height, model);
		}

	}

}
