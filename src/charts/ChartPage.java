package charts;

import java.util.Random;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import tabs.DraggableTab;

public class ChartPage {

	private ObservableList<XYChart.Series<String, Number>> barChartSeries;
	private XYChart.Series<Number, Number> rmseChartSeries;
	private LineChart<Number, Number> rmseChart;
	private XYChart.Series<Number, Number> deltaRmseChartSeries;
	private LineChart<Number, Number> deltaRmseChart;
	private XYChart.Series<Number, Number> logRegRmseChartSeries;
	private LineChart<Number, Number> logRmseChart;
	private double lastItem;
	BarChart<String, Number> bc;
	final double SCALE_DELTA = 1.1;

	private static int iteration;

	private FlowPane setUpFloawPane() {
		FlowPane flow = new FlowPane();
		flow.setPadding(new Insets(5, 0, 5, 0));
		flow.setVgap(4);
		flow.setHgap(4);
		return flow;
	}

	public FlowPane addWeightHistogram(float[] data) {
		FlowPane flow = setUpFloawPane();

		final CategoryAxis xAxis = new CategoryAxis();
		final NumberAxis yAxis = new NumberAxis();
		bc = new BarChart(xAxis, yAxis);
		bc.setTitle("Weight distribution summary");
		xAxis.setLabel("Value");
		yAxis.setLabel("Number of element");

		Bounds bounds = findBounds(data);
		barChartSeries = createWeightHistogram(data, bounds.min, bounds.max, 10);
		bc.setData(barChartSeries);

		ImageView pages[] = new ImageView[8];
		for (int i = 0; i < 1; i++) {
			flow.getChildren().add(bc);
		}

		return flow;
	}

	public FlowPane addRmseChart(float[] data) {
		FlowPane flow = setUpFloawPane();

		rmseChartSeries = new XYChart.Series<Number, Number>();
		rmseChart = createLineChartWithParam("RMSE", "Iteration", "RMSE", rmseChartSeries);

		ScrollPane scroll = new ScrollPane();
		scroll.setContent(rmseChart);
		scroll.setFitToHeight(true);
		flow.getChildren().add(scroll);

		return flow;
	}

	public FlowPane addDeltaRmseChart(float[] data) {
		FlowPane flow = setUpFloawPane();

		deltaRmseChartSeries = new XYChart.Series<Number, Number>();
		deltaRmseChart = createLineChartWithParam("Delta RMSE", "Iteration", "Delta RMSE", deltaRmseChartSeries);

		ScrollPane scroll = new ScrollPane();
		scroll.setContent(deltaRmseChart);
		scroll.setFitToHeight(true);
		flow.getChildren().add(scroll);

		return flow;
	}

	public FlowPane addLogRegRmseChart(float[] data) {
		FlowPane flow = setUpFloawPane();

		logRegRmseChartSeries = new XYChart.Series<Number, Number>();
		logRmseChart = createLineChartWithParam("LogReg RMSE", "Iteration", "RMSE", logRegRmseChartSeries);

		ScrollPane scroll = new ScrollPane();
		scroll.setContent(logRmseChart);
		scroll.setFitToHeight(true);
		flow.getChildren().add(scroll);

		return flow;
	}

	public LineChart createLineChartWithParam(String title, String xAxisStr, String yAxisStr, XYChart.Series series) {
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		LineChart chart = new LineChart<Number, Number>(xAxis, yAxis);
		chart.setTitle(title);
		xAxis.setLabel(xAxisStr);
		yAxis.setLabel(yAxisStr);
		chart.getData().addAll(series);

		chart.setOnScroll(new EventHandler<ScrollEvent>() {
			public void handle(ScrollEvent event) {
				event.consume();

				if (event.getDeltaY() == 0) {
					return;
				}

				double scaleFactor = (event.getDeltaY() > 0) ? SCALE_DELTA : 1 / SCALE_DELTA;

				chart.setScaleX(chart.getScaleX() * scaleFactor);
				chart.setScaleY(chart.getScaleY() * scaleFactor);
			}
		});

		chart.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (event.getClickCount() == 2) {
					chart.setScaleX(1.0);
					chart.setScaleY(1.0);
				}
			}
		});

		return chart;

	}

	public void addDataToLineChart(double value, int iter) {
		Data<Number, Number> data = new Data<Number, Number>(iter, value);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				rmseChartSeries.getData().add(data);
			}
		});

	}

	public void addDataToDeltaRmseChart(double value, int iter) {
		double v = 0;
		if (iter != 1) {
			v = lastItem - value;
		}
		Data<Number, Number> data = new Data<Number, Number>(iter, v);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				deltaRmseChartSeries.getData().add(data);
			}
		});
		lastItem = value;

	}

	public void addDataToLogRegLineChart(double value, int iter) {
		Data<Number, Number> data = new Data<Number, Number>(iter, value);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				logRegRmseChartSeries.getData().add(data);
			}
		});

	}

	public void addNewLineChart() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (rmseChartSeries != null) {
					rmseChartSeries = new XYChart.Series<Number, Number>();
					rmseChart.getData().addAll(rmseChartSeries);
				}
				if (deltaRmseChartSeries != null) {
					deltaRmseChartSeries = new XYChart.Series<Number, Number>();
					deltaRmseChart.getData().addAll(rmseChartSeries);
				}
			}
		});

	}

	public void addNewLogRegLineChart() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (logRegRmseChartSeries != null) {
					logRegRmseChartSeries = new XYChart.Series<Number, Number>();
					logRmseChart.getData().addAll(logRegRmseChartSeries);
				}
			}
		});
	}

	public static class Bounds {
		float max;
		float min;

		public Bounds(float max, float min) {
			super();
			this.max = max;
			this.min = min;
		}

	}

	public static Bounds findBounds(float[] array) {
		float max = array[0];
		float min = array[0];
		for (int i = 1; i < array.length; i++) {
			float current = array[i];
			if (current > max) {
				max = current;
			} else if (current < min) {
				min = current;
			}
		}
		return new Bounds(max, min);
	}

	public void recalculateHistogram(float[] data, int bins) {
		if (data != null) {
			Bounds bounds = findBounds(data);
			barChartSeries = createWeightHistogram(data, bounds.min, bounds.max, bins);
			bc.setData(barChartSeries);
		}
	}

	private ObservableList<XYChart.Series<String, Number>> createWeightHistogram(float[] data, float min, float max, int bins) {
		int[] result = calculateHistogramData(data, 0, 1, 10);
		ObservableList<XYChart.Series<String, Number>> list = FXCollections.observableArrayList();
		XYChart.Series series1 = new Series();
		series1.setName("Weight distribution");
		float[] boundaries = new float[bins + 1];
		for (int i = 0; i < bins; i++) {
			boundaries[i] = min + max / (float) bins * (i + 1);
			series1.getData().add(new XYChart.Data(Float.toString(boundaries[i]), result[i]));
		}
		list.add(series1);
		return list;

	}

	private int[] calculateHistogramData(float[] data, float min, float max, int bins) {
		float[] boundaries = new float[bins + 1];
		for (int i = 0; i < bins + 1; i++) {
			boundaries[i] = min + max / (float) bins * i;
		}

		int[] result = new int[bins];
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < boundaries.length - 1; j++) {
				if (boundaries[j] < data[i] && boundaries[j + 1] > data[i]) {
					result[j]++;
				}
			}
		}

		return result;

	}

	/**
	 * Initialize normal distributed weight.
	 */
	public float[] initializeNormalDistributedData(int size) {
		Random r = new Random();
		float[] data = new float[size];
		for (int i = 0; i < data.length; i++) {
			float random = (float) (r.nextGaussian() * 0.5 + 0.5);
			data[i] = random;
		}
		return data;
	}

	public static class Test extends Application {
		public static void start(String[] args) {
			launch(args);
		}

		@Override
		public void start(final Stage primaryStage) {
			DraggableTab tab = new DraggableTab("Tab 2");
			tab.setClosable(false);
			ChartPage charts = new ChartPage();

			float[] data = charts.initializeNormalDistributedData(10000);
			Node node = charts.addWeightHistogram(data);
			Node node2 = charts.addRmseChart(data);
			Node node3 = charts.addDeltaRmseChart(data);
			Node node4 = charts.addLogRegRmseChart(data);

			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(0, 10, 0, 10));

			grid.add(node, 0, 0);
			grid.add(node2, 1, 0);
			grid.add(node3, 2, 0);
			grid.add(node4, 0, 1);

			tab.setContent(grid);
			TabPane tabs = new TabPane();
			tabs.getTabs().add(tab);

			Runnable r = new Runnable() {

				@Override
				public void run() {

					Random rand = new Random();
					for (int i = 0; i < 20; i++) {
						try {
							System.out.println(i);
							double value = rand.nextDouble();
							charts.addDataToLineChart(value, iteration);
							charts.addDataToDeltaRmseChart(value, iteration);
							charts.addDataToLogRegLineChart(value, iteration);
							Thread.sleep(200);
							iteration++;
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}

			};
			Thread t = new Thread(r);
			t.start();

			StackPane root = new StackPane();
			root.getChildren().add(tabs);

			Scene scene = new Scene(root);

			primaryStage.setScene(scene);
			primaryStage.show();

		}
	}

	public static void main(String[] args) {
		ChartPage chartPage = new ChartPage();
		float[] data = chartPage.initializeNormalDistributedData(10000);
		int[] result = chartPage.calculateHistogramData(data, 0, 1, 10);
		for (int i = 0; i < result.length; i++) {
			System.out.println(result[i]);
		}

		Test.start(args);

	}

}
