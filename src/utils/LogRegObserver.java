package utils;

import javafx.application.Platform;
import javafx.scene.control.ProgressBar;
import view.WriteNumbersModel;

import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.Subject;
import com.bme.datamining.logging.observer.simpleobserver.LogisticRegressionResultObject;

public class LogRegObserver implements Observer {
	private Subject topic;
	ProgressBar progressbar;
	int maxIterations;
	WriteNumbersModel model;

	public LogRegObserver(WriteNumbersModel model) {
		this.progressbar = model.getView().getParameterPanel().getProgressBar();
		this.maxIterations = model.getIterationNumber();
		this.model = model;
	}

	@Override
	public void update() {
		LogisticRegressionResultObject msg = (LogisticRegressionResultObject) topic.getUpdate(this);
		if (msg == null) {
			System.out.println(" No new message");
		} else {
			printResult(msg);
			model.setWeight(msg.getModel());
			// model.showSeperatedModel(msg.getModel());
		}
	}

	public void printResult(LogisticRegressionResultObject msg) {
		maxIterations = 10000;
		double percent = (double) msg.getIterationNumber() / maxIterations;
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				progressbar.setProgress(percent);
				model.calculateWeightHistogram();
			}
		});
		model.addElementToLogRegResultGraph(msg.getError(), msg.getIterationNumber());
		if (percent == 1) {
			model.addNewSeriesToLogRegResultGraph();
		}
	}

	public void showViewModel() {
	}

	@Override
	public void setSubject(Subject sub) {
		this.topic = sub;
	}
}
