package utils;

public class MathUtils {
	public static float getMax(float[] array) {
		float max = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}

		}
		return max;
	}

	public static float getMin(float[] array) {
		float min = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}

	public static float[] normalizeArray(float[] array) {
		float min = getMin(array);
		float max = getMax(array);
		float[] ret = new float[array.length];
		for (int i = 0; i < array.length; i++) {
			float last = array[i];
			ret[i] = (last + Math.abs(min)) / (Math.abs(max) + Math.abs(min));
			// Math.abs(min) + " / " + max);
		}
		return ret;
	}

	public static float[] normalizeWithValues(int[] array, int max, int min) {
		float[] ret = new float[array.length];
		for (int i = 0; i < array.length; i++) {
			float last = array[i];
			ret[i] = (last + Math.abs(min)) / (Math.abs(max) + Math.abs(min));

		}
		return ret;
	}

	public static float[] normalizeWithValues(float[] array, int max, int min) {
		float[] ret = new float[array.length];
		for (int i = 0; i < array.length; i++) {
			float last = array[i];
			ret[i] = (last + Math.abs(min)) / (Math.abs(max) + Math.abs(min));

		}
		return ret;
	}

	public static float[] filterByPositiveValues(float[] arr, float treshold) {
		float[] ret = new float[arr.length];
		for (int i = 0; i < arr.length; i++) {
			float last = arr[i];
			if (last < treshold) {
				ret[i] = 0;
			} else {
				ret[i] = last;
			}

		}
		return ret;
	}

	public static float[][] seperateArray(float[] array, int hidden) {
		float[][] values = new float[hidden][array.length / hidden];
		for (int i = 0; i < array.length; i += hidden) {
			for (int j = 0; j < hidden; j++) {
				values[j][i / hidden] = array[i + j];
			}

		}
		return values;
	}

}
