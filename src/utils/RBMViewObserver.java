package utils;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.control.ProgressBar;
import view.WriteNumbersModel;

import com.bme.datamining.logging.observer.Observer;
import com.bme.datamining.logging.observer.Subject;
import com.bme.datamining.logging.observer.simpleobserver.RBMResultObject;

public class RBMViewObserver implements Observer {

	private Subject topic;
	ProgressBar progressbar;
	int maxIterations;
	WriteNumbersModel model;
	List<RBMResultObject> result;

	public RBMViewObserver(WriteNumbersModel model) {
		this.progressbar = model.getView().getParameterPanel().getProgressBar();
		this.maxIterations = model.getIterationNumber();
		this.model = model;
		this.result = new ArrayList<>();
	}

	@Override
	public void update() {
		RBMResultObject msg = (RBMResultObject) topic.getUpdate(this);
		if (msg == null) {
			System.out.println(" No new message");
		} else {
			printResult(msg);
			// model.setWeight(msg.getModel());
			// model.showSeperatedModel(msg.getModel());
		}
	}

	public void printResult(RBMResultObject msg) {
		result.add(msg);
		double percent = (double) msg.getIterationNumber() / maxIterations;
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				progressbar.setProgress(percent);
				model.calculateWeightHistogram();
			}
		});
		model.addElementToResultGraph(msg.getError(), msg.getIterationNumber());
		if (percent == 1) {
			model.addNewSeriesToResultGraph();
			model.addElementToResultList(result);
			result = new ArrayList<>();
		}
	}

	public void showViewModel() {
	}

	@Override
	public void setSubject(Subject sub) {
		this.topic = sub;
	}

}
